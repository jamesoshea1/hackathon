<template>
    <div class="result__container">
		<a :href="url" target="blank">
			<img :src="image" class="result__image">
			<div
				v-if="price"
				class="result__price"
				v-text="price"
			/>
			<div v-if="date">
				<h5 class="result__info" v-text="dateRepresentation" />
			</div>
			<div v-if="category">
				<h5 class="result__info" v-text="categoryRepresentation" />
			</div>
			<h5 v-text="name" />
			<div v-if="rating && reviewCount">
				<component :is="svgDict[rating]" />
				<h6 class="result__review-count" v-text="reviewCountRepresentation" />
			</div>
		</a>
	</div>
</template>

<script>
import Zero from "../assets/0.svg";
import ZeroPointFive from "../assets/0.5.svg";
import One from "../assets/1.svg";
import OnePointFive from "../assets/1.5.svg";
import Two from "../assets/2.svg";
import TwoPointFive from "../assets/2.5.svg";
import Three from "../assets/3.svg";
import ThreePointFive from "../assets/3.5.svg";
import Four from "../assets/4.svg";
import FourPointFive from "../assets/4.5.svg";
import Five from "../assets/5.svg";

export default {
	name: "Result",
	components: {
		Zero,
		ZeroPointFive,
		One,
		OnePointFive,
		Two,
		TwoPointFive,
		Three,
		ThreePointFive,
		Four,
		FourPointFive,
		Five,
	},
	props: {
		image: { type: String, required: true },
		date: { type: String },
		name: { type: String, required: true },
		url: { type: String, required: true },
		price: { type: String },
		rating: { type: Number },
		reviewCount: { type: Number },
		category: { type: String },
	},
	data() {
		return {
			svgDict: {
				"0": Zero,
				"0.5": ZeroPointFive,
				"1": One,
				"1.5": OnePointFive,
				"2": Two,
				"2.5": TwoPointFive,
				"3": Three,
				"3.5": ThreePointFive,
				"4": Four,
				"4.5": FourPointFive,
				"5": Five,
			},
		};
	},
	computed: {
		categoryRepresentation() {
			return this.category.split(", ").join(" · ");
		},
		dateRepresentation() {
			return `${new Date(this.date).toLocaleDateString("en-GB", {
				month: "long",
				day: "numeric",
			})} ${new Date(this.date).toLocaleTimeString("en-US", {
				hour: "numeric",
				minute: "numeric",
			})}`;
		},
		reviewCountRepresentation() {
			return `(${this.reviewCount})`;
		},
	},
};
</script>

<style lang="scss" scoped>
a {
	text-decoration: none;
	color: initial;
}
h5 {
	margin: 0;
}
.result__review-count {
	margin: 0;
	display: inline;
	color: #505050;
	padding: 0 0 0 0.5em;
	line-height: 18px;
}
.result__container {
	position: relative;
	width: 150px;
	padding: 1em 0.5em 1em;
}
.result__image {
	width: 150px;
	height: 75px;
	margin: 0 0 0.25em 0;
	border-radius: 5px;
}
.result__info {
	font-family: "Open Sans";
	margin: 0.25em 0 0;
	color: #505050;
	line-height: 18px;
	font-size: 0.6em;
	white-space: nowrap;
	text-overflow: ellipsis;
	overflow: hidden;
	width: 150px;
}
.result__price {
	position: absolute;
	top: 80px;
	right: 30px;
	height: 20px;
	text-align: center;
	border-radius: 10px;
	background: white;
	font-weight: bold;
	font-size: 0.8em;
	padding: 5px 0.75em 0;
}
</style>
